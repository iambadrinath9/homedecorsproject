package com.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.Model.Product;

@Repository
public interface ProductRepository extends JpaRepository<Product, Integer>{

	Product getByName(String name);

	Product getByImageurl(String imageurl);
	
	
	@Query("select m from Product m where m.category like concat( '%',:cname,'%')")
	public List<Product> getproductsbyCategory(@Param("cname") String cname);
	
	@Query("select m from Product m where m.name like concat( '%',:cname,'%')")
	public List<Product> getproductsbyName(@Param("cname") String cname);




}
